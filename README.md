# Training

## Welcome to Gitlab Training Session-1

## Getting started

To make it easy for you to get started with GitLab, here's a list of next steps.

## Reference

- [ ] [Gitlab Documentation](https://docs.gitlab.com/) files
- [ ] [Gitlab Tutorials](https://docs.gitlab.com/ee/tutorials/) 

```
cd existing_repo
git remote add origin https://gitlab.com/harshitha_r/training.git
git branch -M main
git push -uf origin main
```

## Agenda

- [ ] [Introduction to Version Control Systems (VCS) and Gitlab]
- [ ] [Introduction to Markdown basics]
- [ ] [Setting Up a GitLab Account and Repository]
- [ ] [Working with Repositories]
- [ ] [Branching and Merging]
- [ ] [Merge Requests and Code Reviews]
- [ ] [Continuous Integration/Continuous Deployment (CI/CD)]
- [ ] [GitLab CI/CD Configuration]
- [ ] [GitLab Runner]
- [ ] [Best Practices and Tips]

***Thank You***
